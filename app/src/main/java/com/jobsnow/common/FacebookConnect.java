package com.jobsnow.common;

import com.test_2.R;

import java.util.ArrayList;
import java.util.List;

public class FacebookConnect {

    public static final int FACEBOOK    = R.drawable.my_ic_facebook;


    public static String getName(int id) {
        if (FACEBOOK    == id)return "Facebook";

        return null;
    }

    public static String getLink(int id, String userID) {

        String link = "https://www.";

        if (FACEBOOK    == id)return link + "facebook.com/"    + userID;

        return null;
    }

    public static List<Integer> getSocialMediaIDs() {
        List<Integer> mediaIDs = new ArrayList();
        mediaIDs.add(FACEBOOK);

        return mediaIDs;
    }

}
